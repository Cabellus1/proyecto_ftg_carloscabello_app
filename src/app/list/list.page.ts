import { ConnectionBBDDService } from './../service/connection-bbdd.service';
import { CallNumber } from '@ionic-native/call-number/ngx';

import {
  Component,
  OnInit
} from '@angular/core';

import {
  ImagePicker
} from '@ionic-native/image-picker/ngx';
import {
  MenuController,
  LoadingController,
  NavController
} from '@ionic/angular';
// declare var SqlServer: any;  
import {
  HttpClient,
  HttpHeaders,
  HttpRequest
} from '@angular/common/http';
import {
  EmailComposer
} from '@ionic-native/email-composer/ngx';
import {
  WebView
} from '@ionic-native/ionic-webview/ngx';
// declare var SqlServer : any;

import {
  AlertController
} from '@ionic/angular';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import {
  Camera,
  CameraOptions
} from '@ionic-native/camera/ngx';



@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {


  images: any;
  optionsimage = {
    // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
    // selection of a single image, the plugin will return it.
    maximumImagesCount: 2,

    // max width and height to allow the images to be.  Will keep aspect
    // ratio no matter what.  So if both are 800, the returned image
    // will be at most 800 pixels wide and 800 pixels tall.  If the width is
    // 800 and height 0 the image will be 800 pixels wide if the source
    // is at least that wide.
    width: 800,
    height: 800,

    // quality of resized image, defaults to 100
    quality: 100,

    // output type, defaults to FILE_URIs.
    // available options are 
    // window.imagePicker.OutputType.FILE_URI (0) or 
    // window.imagePicker.OutputType.BASE64_STRING (1)
    outputType: 1
  };
  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }
  image: string;
  constructor(private callNumber: CallNumber, private webView: WebView, public navCtrl: NavController, public http: HttpClient, private menu: MenuController, private loadingCtrl: LoadingController, public imagePicker: ImagePicker, private camera: Camera, public alertController: AlertController, private router: Router, private connectionBBDD:ConnectionBBDDService) {
    
   connectionBBDD.ObtenerArrasterHoy()
    if(localStorage.getItem("User")==null){
      this.router.navigate(["login"]);

    }
    if(localStorage.getItem("Almacen")==null){
      this.router.navigate(["login"]);

    }
  }


  // 
  // }
  ngOnInit() {
    if(localStorage.getItem("User")==null){
      this.router.navigate(["login"]);

    }
    if(localStorage.getItem("Almacen")==null){
      this.router.navigate(["login"]);

    }
  }
 

  async OnClickExpedicion(y: any) {

    
    const alert = await this.alertController.create({
      cssClass: 'alertDanger',
      buttons: [{
          text: 'Entregado',
          cssClass: 'sc-ion-alert-ios',
          handler: () => {
            this.AlertGrafica(y)    
          }
        }, {
          text: 'Ver mas',
          cssClass: 'sc-ion-alert-ios',
          handler: () => {
            sessionStorage.setItem("arrastre_ver", JSON.stringify(y))
            this.router.navigateByUrl('more-details');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'sc-ion-alert-ios',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }
      ]
    });

    await alert.present();
  
  }

  async AlertGrafica(y:any) {
    const alert = await this.alertController.create({

      buttons: [{
        text: 'Sin Grafica',

        handler: () => {
          console.log('Confirm Cancel');
          console.log(y.idArrastre)
          
          
         this.connectionBBDD.CerrarEstado(y.idArrastre, null)

        }
      }, {
        text: 'Con Grafica',
        handler: () => {
          sessionStorage.setItem("idarrastre",y.idArrastre)
          this.router.navigateByUrl('con-entregado');
        
        }
      }]
    });

    await alert.present();
  }

  





  openCustom() {
    console.log("hola");
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  
  OnCLickllamar(){
    this.callNumber.callNumber("670425990", true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
}