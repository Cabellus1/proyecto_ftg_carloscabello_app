export const INCIDENCIAS = [
    {
        id:1,
        contenido:"El vehículo no ha llegado"
    },
    {
        id:2,
        contenido:"El vehículo no ha llegado a la hora prevista"
    },
    {
        id:3,
        contenido:"El vehículo no tiene la grafica de temperatura disponible"
    },
    {
        id:4,
        contenido:"El conductor se niega ha mostrar la grafica de temperatura"
    },
    {
        id:5,
        contenido:"La matricula no corresponde con los datos"
    },
    {
        id:6,
        contenido:"El contenido no es el esperado"
    }
   
]