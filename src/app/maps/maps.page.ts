import { CAMIONES } from './../data/data.camiones';

import { MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
declare var google;



@Component({
  selector: 'app-maps',
  templateUrl: './maps.page.html',
  styleUrls: ['./maps.page.scss'],
})
export class MapsPage implements OnInit {
  mapRef = null;

  camiones_hoy=CAMIONES
  constructor(private menu: MenuController, private loadingCtrl:LoadingController) {

      this.loadMap()
   }

  ngOnInit() {
  }

  async loadMap() {
   
    const loading = await this.loadingCtrl.create();
    loading.present();
    const myLatLng = {
      lat: 39.469861,
      lng: -0.376283
    }
    const mapEle: HTMLElement = document.getElementById('map');
    this.mapRef = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 10
    });
    google.maps.event
    .addListenerOnce(this.mapRef, 'idle', () => {
      loading.dismiss();
      // this.lat=myLatLng.lat;
      // this.lng=myLatLng.lng;
      
     for(let y of this.camiones_hoy){
      this.addMaker(y.coordenadas.x, y.coordenadas.y, y.nombre);

     }

    });
    
  }

  OnClickCamion(y:any){
    // var map = new google.maps.Map(document.getElementById('map'), {
    //   zoom: 20,
    //   center: {lat: y.coordenadas.x, lng: y.coordenadas.y},
    //   disableDefaultUI: true
    // });
    // console.log(y.coordenadas.x)
    // google.maps.LatLng(y.coordenadas.x, y.coordenadas.y)
    const center = new google.maps.LatLng(y.coordenadas.x, y.coordenadas.y);

    const myLatLng = {
      lat: y.coordenadas.x,
      lng: y.coordenadas.y
    }

    this.mapRef.panTo(center);
  }

  private addMaker(lat: number, lng: number, nombre:string) {
    const marker = new google.maps.Marker({
      position: { lat, lng },
      map: this.mapRef,
      draggable: false,
      animation: google.maps.Animation.DROP,
      type: 'parking',
      label: nombre,

      title: nombre
    });
    

  }
  openCustom() {
    console.log("hola");
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
}
