import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { ConnectionBBDDService } from './../service/connection-bbdd.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpClient,
  HttpHeaders,
  HttpRequest
} from '@angular/common/http';
import { LoadingController, MenuController, AlertController } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loading: any;
  input_password:string="";
  user_google:any;
  input_user:string="";
  color_user:string="dark";
  color_password:string="dark";

  type_input:string="password";

  icono:boolean=true;
  constructor(private http:HttpClient, public loadingController:LoadingController, private router:Router, private menu:MenuController, private coneccionbbdd:ConnectionBBDDService) { 
    if(localStorage.getItem("User")!=null && localStorage.getItem("Almacen")!=null){
      this.router.navigate(["list"]);
    }
    this.coneccionbbdd.ObteneToken()
    this.menu.enable(false);

   }

   ionViewWillEnter(){
    this.menu.enable(false);
   } 
  ngOnInit() {
    this.menu.enable(false);
  }


  OnClickIcon(x){
    this.icono=x;
    console.log(x)

    if(x){
      this.type_input="password"
    }else{
      this.type_input="text"
    }
  }

  
  async OnClicklogin(){
       
    if(this.input_user!="" && this.input_password!=""){
     
      this.color_user="dark";
      this.color_password="dark";       
      let loading = await this.loadingController.create({});
      loading.present();  
      let numero=-1
      let contenido="/ObtenerAlmacenControl?AppUsuario="+this.input_user+"&AppPassword="+this.input_password
      let token='Basic ' + localStorage.getItem('token')
      let headers = new HttpHeaders({'Authorization': token});
      

      this.http.get(this.coneccionbbdd.url+contenido,{headers: headers})
      .subscribe((data: any) => {
        
          console.log("almacen")        
            console.log("alsadasdmacen")

        console.log(data)
        let datos_user=data

        console.log(data.AlmacenControl)
        if(data.AlmacenControl!=0){
          let contenido_almacen="/ObtenerAlmacenNombre?Codigo="+data.AlmacenControl
          this.http.get(this.coneccionbbdd.url+contenido_almacen,{headers: headers})
          .subscribe((data:any) => {
              console.log(data)
              
              localStorage.setItem("NameUser", this.input_user)
              localStorage.setItem("User", JSON.stringify(datos_user))
              localStorage.setItem("Almacen", data.Almacen)
              this.input_password=""
              this.input_user=""
              this.router.navigate(["list"]);

              
  //             let email=require('mailer')
  //   email.send({
  //     host : "smtp.gmail.com",
  //     port : "587",
  //     ssl : true,
     
  //     to : "carlitoscabellus@gmail.com",
  //     from : "geos.envios@gmail.com",
  //     subject : "Mailer library Mail node.js",
  //     text: "Mail by Mailer library",
  //     html: "<span> Hello World Mail sent from  mailer library" ,
  //     authentication : "login",        // auth login is supported; anything else $
  //     username : 'geos.envios@gmail.com',
  //     password : '135798642Ge.'
  //     },
  //     function(err, result){
  //       if(err){ console.log('mal')  }
  //       else { console.log('hurray! Email Sent'); 
  //      }
  // });


              this.coneccionbbdd.InsertarAccionUsuario("INICIO SESION", "APP", 0, "usuario inicia sesion correctamente")
              loading.dismiss();
          },
          (error: any) => {
            alert("Error con la conexion")
            loading.dismiss();
      
          })
    
        }else{
          loading.dismiss();
        }
        numero= data.AlmacenControl
       console.log(numero)
  
      },
      (error: any) => {
        alert("Error con la conexion")
        numero= -1;
        console.log(error)

        console.log(this.coneccionbbdd.url)
        loading.dismiss();

      })
      // this.router.navigate(["list"]);
    
      console.log(numero)
     
       
    }else{
      if(this.input_user==""){
        this.color_user="danger";
      }else{
        this.color_user="dark";
      }
      if(this.input_password==""){
        this.color_password="danger";
      }else{
        this.color_password="dark";
      }
        
    }
    
  }

 

}
