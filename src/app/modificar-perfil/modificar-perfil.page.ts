import { ConnectionBBDDService } from './../service/connection-bbdd.service';
import { Component, OnInit } from '@angular/core';
import {
  AlertController
} from '@ionic/angular';
import {
  MenuController,
  LoadingController,
  NavController
} from '@ionic/angular';
@Component({
  selector: 'app-modificar-perfil',
  templateUrl: './modificar-perfil.page.html',
  styleUrls: ['./modificar-perfil.page.scss'],
})
export class ModificarPerfilPage implements OnInit {
  icono_new:boolean=true;

  icono_repeat:boolean=true;

  type_input_password:string="password"
  type_input_repeat_password:string="password"

  new_user:string=""
  new_password:string=""
  repeat_new_password:string=""

  constructor(public alertController: AlertController, private menu:MenuController, private connectionBBDD:ConnectionBBDDService) { }

  ngOnInit() {
  }
  openCustom() {
    console.log("hola");
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  OnClickIcon_password(x){
    this.icono_new=x;
    console.log(x)

    if(x){
      this.type_input_password="password"
    }else{
      this.type_input_password="text"
    }
  }

  OnClickIcon_repeat(x){
    this.icono_repeat=x;
    console.log(x)

    if(x){
      this.type_input_repeat_password="password"
    }else{
      this.type_input_repeat_password="text"
    }
  }

  OnClickModificar(){
    this.Confirmacion_modificacion()
    console.log(this.new_user)
    console.log(this.new_password)

    console.log(this.repeat_new_password)


    if (this.new_user.trim() && this.new_password.trim() && this.repeat_new_password.trim()){

      if(this.new_password==this.repeat_new_password){

        this.Confirmacion_modificacion()
       
        
      }else{
        console.log("La constraña no incide")
      }


     
    }else{
      

    }
  }

  async Confirmacion_modificacion() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¿Estas seguro que quieres cambiar el usuario y contraseña?',
      message: 'Si cambias tu usuario y contraseña se cerrará tu sesion y tendras que iniciar sesion con tu nuevo nombre de usuario y contraseña',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.new_password=""
            this.new_user=""
            this.repeat_new_password=""
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            let id=JSON.parse(localStorage.getItem("User")).Codigo
            console.log(id)
    
            
            this.connectionBBDD.ModificarPerfil(this.new_user,this.new_password,id)
            this.new_password=""
            this.new_user=""
            this.repeat_new_password=""
          }
        }
      ]
    });

    await alert.present();
  }
}
