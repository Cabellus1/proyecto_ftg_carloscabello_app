import { TestBed } from '@angular/core/testing';

import { ConnectionBBDDService } from './connection-bbdd.service';

describe('ConnectionBBDDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConnectionBBDDService = TestBed.get(ConnectionBBDDService);
    expect(service).toBeTruthy();
  });
});
