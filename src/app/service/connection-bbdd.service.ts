import { element } from 'protractor';
import { Injectable, ɵConsole } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpRequest
} from '@angular/common/http';
import { Router, NavigationExtras   } from '@angular/router';

import {LoadingController, MenuController } from '@ionic/angular';
import { DatePipe } from '@angular/common';

import { groupBy } from 'rxjs/internal/operators/groupBy';
@Injectable({
  providedIn: 'root'
})
export class ConnectionBBDDService {
  loading: any; 
  almacen:string;
  public bultos_kilos=[]
  public bultos_kilos_hoy=[]
  public bultos_kilos_fecha=[]
  public url:string
  public arrastres_hoy
  public arrastres_fecha
  //MIRAAA
  constructor( private router:Router,private http:HttpClient,public loadingController: LoadingController, public DatePipe:DatePipe) {
   
    this.almacen=localStorage.getItem('Almacen')
   // this.url="";
       this.url="";

    if(localStorage.getItem("User")==null){
      this.router.navigate(["login"]);

    }
    if(localStorage.getItem("Almacen")==null){
      this.router.navigate(["login"]);

    }
    if (localStorage.getItem('token')==null){
     
      this.ObteneToken()
    }else{
     
    }
   
   }

  
  
  async ObteneToken(){
    let loading = await this.loadingController.create({});
    loading.present();
    let contenido=""
        this.http.get(this.url+contenido)
    .subscribe((data: any) => {
      localStorage.setItem("token", data.Token)
      loading.dismiss();

    },
    (error: any) => {
      alert("Error con la conexion")
      loading.dismiss();

    })
   
   }
    login(user:string, pass:string){
    let contenido="/ObtenerAlmacenControl?AppUsuario="+user+"&AppPassword="+pass
    let token='Basic ' + localStorage.getItem('token')
    let headers = new HttpHeaders({'Authorization': token});
    this.http.get(this.url+contenido,{headers: headers})
    .subscribe((data: any) => {
      if(data.AlmacenControl!=0){
        let contenido_almacen="/ObtenerAlmacenNombre?Codigo="+data.AlmacenControl
        this.http.get(this.url+contenido_almacen,{headers: headers})
        .subscribe((data:any) => {
            localStorage.setItem("Almacen", data.Almacen)
        })
      }
      return data.AlmacenControl
    },
    (error: any) => {
      alert("Error con la conexion")
      return -1;
    })
   }

   async ObtenerArrasterFechas(desde:string, hasta:string){
    let loading = await this.loadingController.create({});
    loading.present();    
    let contenido="/ObtenerArrastres?strFechaDesde="+desde+"&strFechaHasta="+hasta+"&Destino="+localStorage.getItem("Almacen")+"&AlmacenControl=1&EstadoArrastre=cerrado"
    let token='Basic ' + localStorage.getItem('token')
    let headers = new HttpHeaders({'Authorization': token});
    this.http.get(this.url+contenido,{headers: headers})
    .subscribe((data: any) => {
      this.arrastres_fecha=this.removeDuplicates(data,'idArrastre')
      for(let y of this.arrastres_fecha){
        this.KilosArrastre(y.idArrastre)
      }
      loading.dismiss();
    },
    (error: any) => {
      alert("Error con la conexion")
      loading.dismiss();
    })
    // this.bultos_kilos=this.bultos_kilos_hoy.concat(this.bultos_kilos_fecha)
   }

  

  async CerrarEstado(idArrastre:any, adjunto:any){
    let loading = await this.loadingController.create({});
    loading.present();
    let postData
     postData = {
        "EstadoArrastre": "cerrado",
        "idArrastre": idArrastre
      }
    this.InsertarAccionUsuario("RECEPCION DE ARRASTRE", "ARRASTRE", idArrastre, "el usuario ha hecho una recepcion sobre un arrastre correctamente")
    let contenido="/ModificarEstadoArrastre"
    let token='Basic ' + localStorage.getItem('token')
     let headers = new HttpHeaders({'Authorization': token});
     console.log(headers)
     this.http.post(this.url+contenido,postData,{headers: headers}).subscribe(data => {
      console.log(data);
      loading.dismiss();
      this.ObtenerArrasterHoy()
     }, error => {
      console.log(error);
    });
  }

  async ObtenerArrasterHoy(){
    var now = new Date();
    let hoy=this.DatePipe.transform(Date.now(),'yyyy-MM-dd')
    let de=this.DatePipe.transform(new Date(now.getFullYear(), now.getMonth(), now.getDate()-14),'yyyy-MM-dd')
    let fecha_inicio=this.DatePipe.transform(de, 'MM/dd/yyyy')
    let fecha_fin=this.DatePipe.transform(hoy, 'MM/dd/yyyy')
   let loading = await this.loadingController.create({});
    loading.present();    
    let contenido="/ObtenerArrastres?strFechaDesde="+fecha_inicio+"&strFechaHasta="+fecha_fin+"&Destino="+localStorage.getItem("Almacen")+"&AlmacenControl=1&EstadoArrastre=ruta"
     let token='Basic ' + localStorage.getItem('token')
     let headers = new HttpHeaders({'Authorization': token});
     this.http.get(this.url+contenido,{headers: headers})
    .subscribe((data: any) => {
      this.arrastres_hoy=this.removeDuplicates(data,'idArrastre')
      for(let y of this.arrastres_hoy){
        this.KilosArrastre(y.idArrastre)
      }
      loading.dismiss();

    },
    (error: any) => {
      alert("Error con la conexion")
      loading.dismiss();

    })

   }
  removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
     return newArray;
}
   
  async KilosArrastre(id:any){
    let contenido="/ObtenerKilosExpediciones?idArrastre="+id
    let token='Basic ' + localStorage.getItem('token')
    let headers = new HttpHeaders({'Authorization': token});

    if(!this.bultos_kilos.includes(id)){
      this.http.get(this.url+contenido,{headers: headers})
      .subscribe((data: any) => {
        
         let todo={
           'id':id,
           'datos':data
         }
         this.bultos_kilos.push(todo)
         
      })
    }
   
  }
 

  async ModificarPerfil(new_user:string, new_password:string, id:any){
    let loading = await this.loadingController.create({});
    loading.present();    
    let postData = {
      "AppUsuario": new_user,
      "AppPassword":new_password,
      "Codigo": id
    }
    let contenido="/ModificarTrabajadoresUsuPass"
    let token='Basic ' + localStorage.getItem('token')
    let headers = new HttpHeaders({'Authorization': token});
    this.http.post(this.url+contenido,postData,{headers: headers})
    .subscribe((data: any) => { 
       console.log(data) 
       loading.dismiss()
       this.InsertarAccionUsuario("USUARIO MODIFICADO", "APP", 0, "el usuario fue modificado correctamente")
       alert("Usuario Modificado, se procede a cerrar sesion")
       localStorage.removeItem("User")
      localStorage.removeItem("Almacen")
       this.router.navigate(["login"]);
    },(error: any) => {
      console.log(error)
      alert(error)
      loading.dismiss()

    })
  }
 
  async EnviarCorreo(){
    let contenido="/ObtenerNumeroAleatorio"
    //EnviarCorreoConCuentaConfiguradaConImagen
    // let contenido="/EnviarCorreoConCuentaConfiguradaConImagen?Origen=geos.envios@gmail.com&Destino=carlitoscabellus@gmail.com&Asunto=PRUEBA&Texto=dasdasdasd"

    let token='Basic ' + localStorage.getItem('token')
    let headers = new HttpHeaders({'Authorization': token});
    this.http.get(this.url+contenido,{headers: headers})
    .subscribe((data: any) => {
      
       console.log(data)
     
       
    },(error: any) => {
      console.log(error)
      alert(error)
    })

  }
  async EnviarIncidencia(incidencia:string, id:any){
    let contenido="/EnviarIncidencia"
    let postdata={
     Inicidencia:incidencia,
     idArrastre:id
     }
    let token='Basic ' + localStorage.getItem('token')
    let headers = new HttpHeaders({'Authorization': token});
    this.http.post(this.url+contenido,postdata,{headers: headers})
    .subscribe((data: any) => {
       console.log(data)
       this.InsertarAccionUsuario("INCIDENCIA REDAPTADA", "ARRASTRE", id, "el usuario enviado una correctamente")
    },(error: any) => {
      console.log(error)
      alert(error)
    })
  }
  async InsertarAccionUsuario(accion:string,element:string, cod_elemento:number, observacion:string){
    let contenido="/InsertarAccionesUsuarios"
    let postdata={
      AccionesUsuarios:{
      Usuario:localStorage.getItem("NameUser"),
      Accion: accion,
      Elemento: element,
      CodigoElemento:cod_elemento,
      Observaciones:observacion}
    }
    let token='Basic ' + localStorage.getItem('token')
    let headers = new HttpHeaders({'Authorization': token});
    this.http.post(this.url+contenido,postdata,{headers: headers})
    .subscribe((data: any) => {
       console.log(data)      
    },(error: any) => {
      console.log(error)
      alert(error)
     

    })

    console.log(postdata)
  }
}
