import { ConnectionBBDDService } from './service/connection-bbdd.service';
import { Component } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Router, NavigationExtras   } from '@angular/router';
import { LoadingController, MenuController, AlertController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  images:any;
  optionsimage = {
    // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
    // selection of a single image, the plugin will return it.
    maximumImagesCount: 2,

    // max width and height to allow the images to be.  Will keep aspect
    // ratio no matter what.  So if both are 800, the returned image
    // will be at most 800 pixels wide and 800 pixels tall.  If the width is
    // 800 and height 0 the image will be 800 pixels wide if the source
    // is at least that wide.
    width: 800,
    height: 800,

    // quality of resized image, defaults to 100
    quality: 100,

    // output type, defaults to FILE_URIs.
    // available options are 
    // window.imagePicker.OutputType.FILE_URI (0) or 
    // window.imagePicker.OutputType.BASE64_STRING (1)
    outputType: 1
};
  constructor(
    private coneccionbbdd:ConnectionBBDDService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public imagePicker: ImagePicker,
    private router:Router,
    private menu:MenuController,
    public alertController: AlertController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  onGaleria(){
    this.optionsimage = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      maximumImagesCount: 1,
  
      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 800,
      height: 800,
  
      // quality of resized image, defaults to 100
      quality: 100,
  
      // output type, defaults to FILE_URIs.
      
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
  };
    this.images=[]

    this.imagePicker.getPictures(this.optionsimage).then(async (results) => {
      for (var i = 0; i < results.length; i++) {
      this.images.push('data:image/jpeg;base64,' + results[i])
    }
      alert(results)
    }, (err) => {
      alert(err);
    });
  }

  
  OnClickRecepcionesPendientes(){
    this.router.navigate(["list"]);
    this.menu.close()
  }
  OnClickUltimasRecepciones(){
    this.router.navigate(["list-historial"]);
    this.menu.close()
  }
  OnClickEnviarIncidencia(){
    this.router.navigate(["incidencia"]);
    this.menu.close()
  }
  OnClickMapa(){
    this.router.navigate(["maps"]);
    this.menu.close()
  }
  OnClickModificarPerfil(){
    this.router.navigate(["modificar-perfil"]);
    this.menu.close()
  }
 async OnClickCerrarSession(){
    
      const alert = await this.alertController.create({
        header: '¿Estas seguro que quieres cerrar sesion?',
        
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Si',
            handler: () => {
              this.coneccionbbdd.InsertarAccionUsuario("CIERRE DE SESION", "APP", 0, "usuario cerrado sesion correctamente")

              localStorage.removeItem("User")
              localStorage.removeItem("Almacen")
              localStorage.removeItem("NameUser")

              this.menu.close()
              this.router.navigate(["login"]);
            }
          }
        ]
      });
  
      await alert.present();
    
  

  }
}
