import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
 
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then( m => m.ListPageModule)
  },
  {
    path: 'list-historial',
    loadChildren: () => import('./list-historial/list-historial.module').then( m => m.ListHistorialPageModule)
  },
  {
    path: 'maps',
    loadChildren: () => import('./maps/maps.module').then( m => m.MapsPageModule)
  },
  
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'more-details',
    loadChildren: () => import('./more-details/more-details.module').then( m => m.MoreDetailsPageModule)
  },
 
  
  {
    path: 'incidencia',
    loadChildren: () => import('./incidencia/incidencia.module').then( m => m.IncidenciaPageModule)
  },
  {
    path: 'con-entregado',
    loadChildren: () => import('./con-entregado/con-entregado.module').then( m => m.ConEntregadoPageModule)
  },
  {
    path: 'modificar-perfil',
    loadChildren: () => import('./modificar-perfil/modificar-perfil.module').then( m => m.ModificarPerfilPageModule)
  },




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
