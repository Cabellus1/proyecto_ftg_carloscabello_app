import { Component, OnInit } from '@angular/core';
import { ConnectionBBDDService } from './../service/connection-bbdd.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

import { LoadingController, MenuController, AlertController } from '@ionic/angular';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Router, NavigationExtras   } from '@angular/router';


@Component({
  selector: 'app-con-entregado',
  templateUrl: './con-entregado.page.html',
  styleUrls: ['./con-entregado.page.scss'],
})
export class ConEntregadoPage implements OnInit {
  base64Image:any
  image:string;
  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }
  constructor(private menu:MenuController, private emailComposer:EmailComposer, private connectionbbdd:ConnectionBBDDService, public camera:Camera, private webView: WebView,private router: Router ) { 
    // this.image=JSON.stringify(sessionStorage.getItem("img"))
    this.image="";
    this.menu.enable(false);

    this.OpenCamera();
  }

  OnClickCancelar(){
    this.router.navigateByUrl('list');

    
  }
  OpenCamera(){
    
    this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.base64Image = imageData;
      this.image = this.webView.convertFileSrc(imageData);

      
      // sessionStorage.setItem("img", JSON.parse(this.image))
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
     }, (err) => {
      // Handle error
      if(this.image==""){
        this.router.navigateByUrl('list');
      }

     });


    

  }
  OnClickConfirmar(){

    let email = {
      to: 'carlitoscabellus@gmail.com',
      cc: 'carlitoscabellus@gmail.com',
     
      attachments: [
        this.base64Image
      ],
      subject: 'Cordova Icons',
      body: 'How are you? Nice greetings from Leipzig',
      isHtml: true
    }
    this.emailComposer.open(email).finally(()=>{
      let id=sessionStorage.getItem("idarrastre")
      this.connectionbbdd.CerrarEstado(id, null).finally(()=>{
        alert("Arrastre Cerrado")
        this.router.navigateByUrl('list');
      })
    });

     
     
  }
  OnClickRepetirOpenCamera(){

    this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.image = this.webView.convertFileSrc(imageData);

      // sessionStorage.setItem("img", JSON.parse(this.image))
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
     }, (err) => {
      // Handle error
     

     });


    

  }
  ngOnInit() {
    this.menu.enable(false);

  }

}
