import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConEntregadoPage } from './con-entregado.page';

const routes: Routes = [
  {
    path: '',
    component: ConEntregadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConEntregadoPageRoutingModule {}
