import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConEntregadoPage } from './con-entregado.page';

describe('ConEntregadoPage', () => {
  let component: ConEntregadoPage;
  let fixture: ComponentFixture<ConEntregadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConEntregadoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConEntregadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
