import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConEntregadoPageRoutingModule } from './con-entregado-routing.module';

import { ConEntregadoPage } from './con-entregado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConEntregadoPageRoutingModule
  ],
  declarations: [ConEntregadoPage]
})
export class ConEntregadoPageModule {}
