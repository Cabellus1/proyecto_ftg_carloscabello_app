import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListHistorialPage } from './list-historial.page';

describe('ListHistorialPage', () => {
  let component: ListHistorialPage;
  let fixture: ComponentFixture<ListHistorialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHistorialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListHistorialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
