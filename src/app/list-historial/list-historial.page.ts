import { Component, OnInit } from '@angular/core';

import { ConnectionBBDDService } from './../service/connection-bbdd.service';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import { MenuController, LoadingController, AlertController, NavController } from '@ionic/angular';
import {
  CalendarModal,
  CalendarModalOptions,
  CalendarComponentOptions,
  DayConfig,
  CalendarResult
} from 'ion2-calendar';
import { ModalController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-list-historial',
  templateUrl: './list-historial.page.html',
  styleUrls: ['./list-historial.page.scss'],
})
export class ListHistorialPage implements OnInit {

  test: DayConfig[]=[];
  public optionsRange: CalendarComponentOptions = {
    //from: new Date(2019, 0, 1),
    from: new Date(2019, 11, 1),
    to: new Date(2020, 2, 15),
    pickMode: 'range',
    daysConfig: this.test
  };
  de:any;
  hasta:any;
  hoy:any;
  menos_mes:any
  prueba:any;

  fecha_inicio:string;
  fecha_fin:string;

  constructor(public alertController: AlertController, private router: Router,private connectionBBDD:ConnectionBBDDService, private menu: MenuController, public modalCtrl: ModalController, public DatePipe:DatePipe) { 
    if(localStorage.getItem("User")==null){
      this.router.navigate(["login"]);

    }
    if(localStorage.getItem("Almacen")==null){
      this.router.navigate(["login"]);

    }
    var now = new Date();
      this.optionsRange.from = new Date(now.getFullYear(), now.getMonth(), now.getDate()-30);
      this.optionsRange.to = new Date(now.getFullYear(), now.getMonth(), now.getDate());

      this.hasta=this.DatePipe.transform(Date.now(),'yyyy-MM-dd')
      this.hoy=this.DatePipe.transform(Date.now(),'yyyy-MM-dd')

      this.de=this.DatePipe.transform(new Date(now.getFullYear(), now.getMonth(), now.getDate()-30),'yyyy-MM-dd')
     
      this.fecha_inicio=this.DatePipe.transform(this.de, 'MM/dd/yyyy')
      this.fecha_fin=this.DatePipe.transform(this.hasta, 'MM/dd/yyyy')
      console.log(connectionBBDD.arrastres_fecha)
      if(connectionBBDD.arrastres_fecha==undefined){
        this.connectionBBDD.ObtenerArrasterFechas(this.fecha_inicio, this.fecha_fin)

      }


      console.log(this.fecha_inicio) 
      console.log(this.fecha_fin)

  }

  OnClickFiltrar(){
    console.log("DE: "+this.fecha_inicio)
    console.log("HASTA: "+this.fecha_fin)
    this.connectionBBDD.ObtenerArrasterFechas(this.fecha_inicio, this.fecha_fin)

  }
  OnClickfecha_inicio($event){
    this.fecha_inicio=this.DatePipe.transform($event.detail.value, 'MM/dd/yyyy')
    console.log("DE: "+this.fecha_inicio)
    console.log("HASTA: "+this.fecha_fin)

  }
  OnClickfecha_fin($event){
    this.fecha_fin=this.DatePipe.transform($event.detail.value, 'MM/dd/yyyy')
    console.log("DE: "+this.fecha_inicio)
    console.log("HASTA: "+this.fecha_fin)

  }

  fecha( $event){
    console.log("de: "+this.de)

    console.log("hasta: "+this.hasta)

  }
  ngOnInit() {
  }
 
  async OnClickExpedicion(contenido: any) {
    const alert = await this.alertController.create({
      cssClass: 'alertDanger',
      buttons: [
           {
          text: 'Ver mas',
          handler: () => {
    
            sessionStorage.setItem("arrastre_ver", JSON.stringify(contenido))
            this.router.navigateByUrl('more-details');

          }
        },
        {
          text: 'Cancel',
          role: 'cancel',

          handler: () => {
            console.log('Confirm Cancel');
          }
        }
      ]
    });

    await alert.present();
  }
  openCustom() {
    console.log("hola");
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
}
