import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListHistorialPageRoutingModule } from './list-historial-routing.module';

import { ListHistorialPage } from './list-historial.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListHistorialPageRoutingModule
  ],
  declarations: [ListHistorialPage]
})
export class ListHistorialPageModule {}
