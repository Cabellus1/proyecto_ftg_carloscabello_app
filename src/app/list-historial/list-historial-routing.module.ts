import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListHistorialPage } from './list-historial.page';

const routes: Routes = [
  {
    path: '',
    component: ListHistorialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListHistorialPageRoutingModule {}
