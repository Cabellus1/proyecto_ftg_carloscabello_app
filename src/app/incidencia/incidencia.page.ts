// import { INCIDENCIAS } from './../data/data.indicencias';
import { Component, OnInit } from '@angular/core';
import { MenuController, LoadingController, NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { ConnectionBBDDService } from './../service/connection-bbdd.service';
import { INCIDENCIAS } from '../data/data.indicencias';


@Component({
  selector: 'app-incidencia',
  templateUrl: './incidencia.page.html',
  styleUrls: ['./incidencia.page.scss'],
})
export class IncidenciaPage implements OnInit {
 
  inci=INCIDENCIAS
  select_incidencia_vehiculo:number=0
  select_incidencia:number=0
  contenido:String="";
  constructor(private connectionBBDD:ConnectionBBDDService, public menu:MenuController, public alertController: AlertController) {
    console.log(connectionBBDD.arrastres_hoy)
    console.log(INCIDENCIAS)
    console.log(this.inci)
   }

  ngOnInit() {
  }
  openCustom() {
    console.log("hola");
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  OnClickEnviar(){


    if(this.select_incidencia!=0 && this.select_incidencia!=7){
      for(let y of this.inci){
        // console.log(y)
        if(this.select_incidencia==y.id){
     
          this.ConfirmacionIncidencia(y.contenido, this.select_incidencia_vehiculo)
        }
      }
    }else{

        if(this.select_incidencia==7){
          if(this.contenido.trim()){
            this.ConfirmacionIncidencia(this.contenido, this.select_incidencia_vehiculo)
          }
        }

    }
    
   
    
  }
  mostrarIncidencia(id){
    for(let y of this.inci){
      // console.log(y)
      if(id==y.id){
   
        return y.contenido
      }
    }
  }
  mostrarMatricula(){
    for(let y of this.connectionBBDD.arrastres_hoy){
      if(y.idArrastre==this.select_incidencia_vehiculo){
        return y.Matricula
      }
    }
  }
  OnClickBorrar(){
    this.select_incidencia_vehiculo=0
    this.select_incidencia=0

    this.contenido="";
  }

  async ConfirmacionIncidencia(contenido, idarrastre) {
    const alert = await this.alertController.create({
      header: '¿Estas seguro que quieres enviar esta incidencia?',
     
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');


          }
        }, {
          text: 'Enviar',
          handler: () => {
            console.log('Enviar incidencia');
           console.log(contenido);
           console.log(idarrastre)
          this.connectionBBDD.EnviarIncidencia(contenido, idarrastre)
           this.presentAlert()
            this.OnClickBorrar()
          }
        }
      ]
    });

    await alert.present();
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Incidencia Enviada',
      
      buttons: ['OK']
    });

    await alert.present();
  }


}
