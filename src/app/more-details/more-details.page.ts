import { ConnectionBBDDService } from './../service/connection-bbdd.service';
import { Component, OnInit } from '@angular/core';
import { LoadingController, MenuController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-more-details',
  templateUrl: './more-details.page.html',
  styleUrls: ['./more-details.page.scss'],
})
export class MoreDetailsPage implements OnInit {

  dato:object={
    totalKilos:5,
    totalBulto:1,
    COD_PROVEEDOR_ARRASTRE_EXPEDICION: 25,
    PROVEEDOR_ARRASTRE_EXPEDICION: 'JUAN SANCHO ANTEQUERA',
    COD_VEHICULO_ARRASTRE_EXPEDICION: 192,
    CODALMACEN_REPARTO_EXPEDICION: 1,
    ALMACEN_REPARTO_EXPEDICION: 'GESTINMEDICA VALENCIA',
    COD_ALMACEN_ARRASTRE: 21,
    FECHA_EXPEDICION: '2019-10-02 00:00:00.000',
  }
  Arrastre_contenido:any
  constructor(public menu:MenuController, public connectionBBDD:ConnectionBBDDService) {
    this.menu.enable(false);

    this.Arrastre_contenido=JSON.parse(sessionStorage.getItem('arrastre_ver'))
   }

  ngOnInit() {
    this.menu.enable(false);

  }
  optenerKilos(id:any){

    for(let y of this.connectionBBDD.bultos_kilos){
      if(y.id==id){
        return y.datos.TotalKilosTC
      }
    }
  }
  optenerBultos(id:any){
   // console.log(id)
    // console.log(this.connectionBBDD.bultos_kilos)
    for(let y of this.connectionBBDD.bultos_kilos){
      if(y.id==id){
        return y.datos.TotalBultosTC
      }
    }
  }
}
